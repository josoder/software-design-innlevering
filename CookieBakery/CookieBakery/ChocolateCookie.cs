﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookieBakery
{
	class ChocolateCookie : Cookie
	{
		public ChocolateCookie(string producer, int nr) : base(producer, nr) { }

		public override string ToString()
		{
			return base.ToString() + " with chocolate";
		}

		
	}
}
