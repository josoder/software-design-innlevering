﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CookieBakery
{
	class Program
	{
		static void Main(string[] args)
		{
			Thread [] threads = new Thread[5];
			// Create a new shop and set the daily limit to 20 cookies
			CookieShop shop = new CookieShop(20);
			// Intantiate the bakers and their threads 
			Baker granny = new Baker(shop, "Granny");
			Baker chuckie = new Baker(shop, "Chuckie");
			threads[0] = new Thread(new ThreadStart(granny.Produce));
			threads[1] = new Thread(new ThreadStart(chuckie.Produce));
			// Instantiate the customers and their threads
			Consumer fred = new Consumer("Fred", shop);
			Consumer ted = new Consumer("Ted", shop);
            Consumer greg = new Consumer("Greg",shop);
			threads[2] = new Thread(new ThreadStart(fred.BuyCookie));
			threads[3] = new Thread(new ThreadStart(ted.BuyCookie));
			threads[4] = new Thread(new ThreadStart(greg.BuyCookie));
			// Start threads
			for (int i = 0; i < 5; i++)
			{
				threads[i].Start();
			}
			while (!shop.IsClosed()){}
			for (int i = 0; i < 5; i++)
			{
				threads[i].Abort();
			}
			Console.ReadKey(); 
		}
	}
}
