﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CookieBakery
{
	/// <summary>
	/// CookieShop is a monitoring class between threads of consumers and bakers.
	/// The class implements logic that makes sure that one cookie is only handed out to one consumer and that 
	/// all access to the cookielist from the bakers are thread-safe.
	/// CookieShop holds the daily maximum amounts of cookies produced and closes after the amount is reached.  
	/// </summary>
	class CookieShop
	{
		private List<Cookie> cookies;
        private static object cookieListLock = new object(); 
		private static object cookieLock = new object();
        private int dailyTotal;
        private int nrProduced = 0;
        private Boolean closed = false;

		// instantiate the cookieshop
		public CookieShop(int dailyTotal)
		{
			cookies = new List<Cookie>();
            this.dailyTotal = dailyTotal; 
		}

		/// <summary>
		/// Add a cookie to the list. 
		/// Uses a lock since it is accessed from other threads. 
		/// </summary>
		/// <param name="c">Cookie c</param>
		public void AddCookie(Cookie c)
		{
            lock (cookieListLock)
            {
                if (nrProduced>=dailyTotal)
                {
                    closed = true;
                    Console.WriteLine("We are closing for today, please come again tomorrow.");
                }
                else
                this.cookies.Add(c);
                nrProduced++; 
            }
		}

		// Check if the limit is reached. 
        public Boolean IsClosed()
        {
            return closed; 
        }

		/// <summary>
		/// Returns and removes a cookie from the list. 
		/// Uses locks to synchronize since it is accessed by other threads. 
		/// Returns null if the cookie list is empty.
		/// </summary>
		/// <returns>Cookie</returns>
		public Cookie SellCookie()
		{
			lock (cookieLock)
			{
				if (cookies.Count<=0) return null;
				else
				{
					Cookie tmp = this.cookies.First();
					cookies.RemoveAt(0);
					return tmp;
				}
			}
		}
	}
}
