﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookieBakery
{
	class SpaceCookie : Cookie
	{
		public SpaceCookie(string producer, int nr) : base(producer, nr){ }

		public override string ToString()
		{
			return base.ToString() + " with a secret ingredient";
		}
	}
}
