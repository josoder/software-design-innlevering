﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CookieBakery
{
	/// <summary>
	/// Class is implementing an object acting as costumers in a shop lining up to buy cookies. 
	/// If it receives a cookie, it will print the name of the cookie recieved and wait for more. 
	/// The Consumer stops when the shop is closed. 
	/// </summary>
	class Consumer
	{
		private String name;
		private CookieShop shop;

		public Consumer(String name, CookieShop shop)
		{
			this.name = name;
			this.shop = shop; 
		}

		public void BuyCookie()
		{
			Cookie c; 
			while (true)
			{
				c = shop.SellCookie();

                if (c != null)
                {
                    String print = this.name + " received " + c.ToString();
                    Console.CursorLeft = Console.BufferWidth - print.Length;
                    Console.WriteLine(print);
                }
				Thread.Sleep(1000);
			}
		}
	}
}
