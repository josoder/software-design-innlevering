﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CookieBakery
{
	/// <summary>
	/// Class acts as a baker and is producing cookies and delivers them to the shop. 
	/// The baker produces four different kinds of cookies. 
	/// </summary>
	class Baker
	{ 
		private CookieShop shop;
		private String name;
		private Random random;
		private int nr;
        
		
		public Baker(CookieShop shop, String name)
		{
			this.shop = shop;
			random = new Random();
			this.name = name;
		}

		public void Produce()
		{
			while (!shop.IsClosed())
			{
				Thread.Sleep(700);
				int type = random.Next(4);
				Cookie c;
				nr++;
				switch (type)
				{
					case 0:
						c = CreateChocolateCookie(); 
						break;
					case 1:
						c = CreatePlainCookie(); 
						break;
					case 2:
						c = CreateSpaceCookie(); 
						break;
					default:
						c = CreateVanillaCookie();
						break;
				}
                if (shop.IsClosed()) break; 
				Console.WriteLine(name + " made cookie #" + nr);
				shop.AddCookie(c);
			}
		}

		public Cookie CreatePlainCookie()
		{
			return new Cookie(name, nr);
		}

		public Cookie CreateChocolateCookie()
		{
			return new ChocolateCookie(name, nr);
		}

		public Cookie CreateSpaceCookie()
		{
			return new SpaceCookie(name, nr);
		}

		public Cookie CreateVanillaCookie()
		{
			return new VanillaCookie(name, nr);
		}
	}
}
