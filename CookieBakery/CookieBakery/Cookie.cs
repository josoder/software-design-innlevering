﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CookieBakery
{
	// Blueprints for cookies 
	class Cookie
	{
		private string producer;
        private int nr; 

		public Cookie(string producer, int nr)
		{
			this.producer = producer;
            this.nr = nr; 
		}

		public virtual string ToString()
		{
			return producer + "´s Sweet cookie #" + this.nr; 
		}

		
	}
}
