﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snake
{
	/// <summary>
	/// Implements the methods used to draw the game to the console
	/// </summary>
    class Canvas
    { 
        
        public Canvas()
        {
			Console.CursorVisible = false;
			Console.Title = "Snejk";
		}


        public void DrawObject(Point p, char c) {
            Console.SetCursorPosition(p.X, p.Y);
            Console.Write(c);
        }

        public void GreenPen()
        {
            Console.ForegroundColor = ConsoleColor.Green; 
        }

        public void YellowPen()
        {
            Console.ForegroundColor = ConsoleColor.Yellow; 
        }
    }
}
