﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snake
{
    class Point : IEquatable<Point>
    {
        public int X; public int Y;

	    #region Props 
		public Point(int x = 0, int y = 0) { X = x; Y = y; }
        public Point(Point input) { X = input.X; Y = input.Y; }
		#endregion

		public bool Equals(Point other)
        {
            if (other == null) return false;
            if (other.X == this.X && other.Y == this.Y) return true;
            else return false;
        }
    }
}
