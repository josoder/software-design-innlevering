﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Schema;

namespace Snake
{
	/// <summary>
	/// Snake holds the positions of the snake and implements the logic to move it.
	/// </summary>
    class Snake
    {
        private int length = 4;
        private List<Point> snake;
        private int currentDirection = 2;
        private Point head;
        private Point firstLink;
        private Point lastLink;
        
        #region Props
        public int CurrentDirection
        {
            get { return currentDirection; }
        }

        public Point FirstLink
        {
            get
            {
                return firstLink;
            }
        }

        public Point LastLink
        {
            get
            {
                return lastLink;
            }
        }

        public int Length
        {
            get { return this.length; }
        }

        #endregion

		/// <summary>
		/// Constructor, adds a new snake with the length of 4 points. 
		/// </summary>
        public Snake()
        {
            this.snake = new List<Point>();
            for (int i = 0; i <= length; i++)
            {
                snake.Add(new Point(10, 10));
            }
            head = new Point(snake.Last());
            lastLink = new Point(snake.First());
            firstLink = new Point(snake[Length - 1]);
        }

		/// <summary>
		/// Change the direction of the snake by creating a new head in a new coordinate. 
		/// </summary>
		/// <param name="direction"></param>
        public void ChangeDirection(int direction)
        {
            switch (direction)
            {
                // 0 = up, 1 = right, 2 = down, 3 = left
                case 0:
                    // cant rotate 180 degrees 
                    if (currentDirection != 2)
                    {
                        currentDirection = Game.Up;
                    }
                    break;
                case 1:
                    if (currentDirection != 3)
                    {
                        currentDirection = Game.Right;
                    }
                    break;
                case 2:
                    if (currentDirection != 0)
                    {
                        currentDirection = Game.Down;
                    }
                    break;
                default:
                    if (currentDirection != 1)
                    {
                        currentDirection = Game.Left;
                    }
                    break;
            }
        }
		/// <summary>
		/// Update the snakes position and move it one point in the current direction 
		/// </summary>
        public void UpdatePosition()
        {
            head = new Point(CreateNewHead());
            snake.Add(head);
            snake.RemoveAt(0);
            lastLink = new Point(snake.First());
            firstLink = new Point(snake[Length - 1]); 
        }
		
		/// <summary>
		/// Add a link to the snake, making it grow. 
		/// </summary>
        public void AddLink()
        {
            firstLink = new Point(head);
            head = new Point(CreateNewHead());
            snake.Add(head); 
            length++; 
        }

		/// <summary>
		/// The process of moving the snake is made by creating a new head. 
		/// </summary>
		/// <returns></returns>
        public Point CreateNewHead()
        {
            Point newHead = new Point(head);
            // 0 = up, 1 = right, 2 = down, 3 = left
            switch (currentDirection)
            {
                case 0:
                    currentDirection = 0;
                    newHead.Y -= 1;
                    break;
                case 1:
                    currentDirection = 1;
                    newHead.X += 1;
                    break;
                case 2:
                    currentDirection = 2;
                    newHead.Y += 1;
                    break;
                default:
                    currentDirection = 3;
                    newHead.X -= 1;
                    break;
            }
            return newHead; 
        }

        /// <summary>
		/// Returns the heads coordinate(the last point in the list).
		/// </summary>
		/// <returns></returns>
        public Point GetHeadPosition()
        {
            return head;
        }

		/// <summary>
		/// Return the list
		/// </summary>
		/// <returns></returns>
        public List<Point> GetSnake()
        {
            return snake;
        }
    }


}
