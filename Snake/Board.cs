﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snake
{
    class Board
    {
        private int width;
        private int height;
        public int RightWall

	    #region Props
		{
            get { return width; }
        }
        
        public int BottomWall
        {
            get { return height; }
        }
		#endregion

		public Board(int width, int height) 
        {
            this.width = width;
            this.height = height; 
        }
    }
}
