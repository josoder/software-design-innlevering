﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Snake
{
	/// <summary>
	/// The gamecontroller creates the game and handles all logic and communication between the different parts of the application. 
	/// </summary>
	class Game
	{
		public const int Up = 0, Right = 1, Down = 2, Left = 3, Escape = 4, Pause = 5;
		private bool gameOver, pause;
		private Canvas canvas;
		private AppleGenerator appleGenerator;
		private Snake snake;
		private Board board;
		private Point apple;
		private CollisionControl collisionControl;
		private Stopwatch stopwath;
		private int current = 2;
		private int newDirection; 


		/// <summary>
		/// Set the width and height to the size of the consolewindow and instantiate required components. 
		/// </summary>
		public Game()
		{
			canvas = new Canvas();
			snake = new Snake();
			board = new Board(Console.WindowWidth, Console.WindowHeight);
			appleGenerator = new AppleGenerator(board.RightWall, board.BottomWall);
			collisionControl = new CollisionControl(board, snake);
			stopwath = new Stopwatch();
			Start();
		}

		/// <summary>
		/// Start the gameloop
		/// </summary>
		/// <returns></returns>
		public void Start()
		{
            stopwath.Start();
			DrawNewApple();
			canvas.DrawObject(snake.GetHeadPosition(), '@'); 
			while (true)
			{
                snake.UpdatePosition();
                if (collisionControl.CheckCrash()) break;
                UpdatePosition();
                Wait();
				if (Console.KeyAvailable)
				{
					int command = GetKey();
					if (command >= 0 && command < 4)
					{
						newDirection = command;
                        snake.ChangeDirection(command); 
					}
					else if(command == Escape)
					{
						break;
					}
					else if (command == Pause)
					{
						pause = !pause; 
					}
				}
				               
				if (collisionControl.CollidedWithApple(apple)) Grow();
			}
		}


		public void Grow()
		{
			snake.AddLink();
			canvas.YellowPen();
			canvas.DrawObject(snake.GetHeadPosition(), '@');
			canvas.DrawObject(snake.FirstLink, '0');
			DrawNewApple();
		}

		public void UpdatePosition()
		{
			canvas.YellowPen();
            canvas.DrawObject(snake.GetHeadPosition(), '@');
            canvas.DrawObject(snake.FirstLink, '0');
            canvas.DrawObject(snake.LastLink, ' ');
		}

		public void DrawNewApple()
		{
			while (collisionControl.CollidedWithApple(apple = appleGenerator.GetRandomApple())) { }
			canvas.GreenPen();
			canvas.DrawObject(apple, '$');
		}

		// Wait for 100 milliseconds
		public void Wait()
		{
			while (stopwath.ElapsedMilliseconds < 100)
			{
			}
			stopwath.Restart();
		}

		/// <summary>
		/// Get input from keyboard
		/// </summary>
		/// <returns> int </returns>
		public int GetKey()
		{
			ConsoleKeyInfo cki = Console.ReadKey(true);
			if (cki.Key == ConsoleKey.Escape) return Escape;
			else if (cki.Key == ConsoleKey.Spacebar) return Pause;
			else if (cki.Key == ConsoleKey.UpArrow) return Up;
			else if (cki.Key == ConsoleKey.RightArrow) return Right;
			else if (cki.Key == ConsoleKey.DownArrow) return Down;
			else if (cki.Key == ConsoleKey.LeftArrow) return Left;
			// command not found
			return -1;
		}


		public static void Main(string[] args)
		{
			new Game();
			Console.ReadKey(); 
		}
	}
}
