﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snake
{
	/// <summary>
	/// The collision controll checks if the snake collided with an obstacle.
	/// </summary>
	class CollisionControl
	{
		private Board board;
		private Snake snake;

		public CollisionControl(Board board, Snake snake)
		{
			this.board = board;
			this.snake = snake;
		}

		/// <summary>
		/// Check if the snake crashed with the wall or its tail.
		/// Returns true if it crashed. 
		/// </summary>
		/// <returns> bool </returns>
		public bool CheckCrash()
		{
			Point head = snake.GetHeadPosition();
			// the snake collided with a wall
			if (head.X <= -1 || head.Y >= board.BottomWall || 
				head.Y <= -1 || head.X >= board.RightWall)
			{
				return true;
			}
            for (int i = 0; i < snake.Length - 1; i++)
            {
                if (head.Equals(snake.GetSnake()[i])) { 
                return true;
            }
			}
			return false;
		}

		/// <summary>
		/// Check if the snake ate an apple
		/// Returns true if it did.  
		/// </summary>
		/// <returns> bool </returns>
		public bool CollidedWithApple(Point apple)
		{
			foreach(Point p in snake.GetSnake()) {
				if(p.Equals(apple)) return true;
			}
			return false;
		}
	}
}