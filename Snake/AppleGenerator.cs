﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snake
{
	/// <summary>
	/// Creates a new apple, at a random spot on the board.
	/// </summary>
	class AppleGenerator
    {
        private int maxX, maxY;
        Random random;

		public AppleGenerator(int maxX, int maxY)
        {
            random = new Random(); 
            this.maxX = maxX;
            this.maxY = maxY;
        }

        public Point GetRandomApple()
        {
            int x = random.Next(0, maxX);
            int y = random.Next(0, maxY);
            return new Point(x, y); 
        }
    }
}
