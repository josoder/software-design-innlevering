﻿using System;
using System.Threading;

/* This simple code is ment to be used as an example of how race conditions may occur. When you run this application
 * several times you will not always get the same output. This shows how the threads "race" in order to increment the
 * shared count and write to the output at the same time. Without any rules given,  one can not foresee the output. 
 */


namespace RaceConditions_example
{
	class MainClass
	{
		private static int count;
		static void Main(string[] args)
		{
			Thread Thread1 = new Thread(winter);
			Thread1.Start();

			Thread Thread2 = new Thread(spring);
			Thread2.Start();

			Thread Thread3 = new Thread(summer);
			Thread3.Start();

			Thread Thread4 = new Thread(fall);
			Thread4.Start();

			Console.ReadLine();
		}

		private static void winter()
		{

			for (count = 0; count < 20; count++)
			{
				Console.Write(" Winter " + "\t");
			}

		}

		private static void spring()
		{

			for (count = 0; count < 20; count++)
			{
				Console.Write(" Spring " + "\t");
			}
		}

		private static void summer()
		{

			for (count = 0; count < 20; count++)
			{
				Console.Write(" Summer " + "\t");
			}
		}

		private static void fall()
		{
			for (count = 0; count < 20; count++)
			{
				Console.Write(" Fall " + "\t");
			}
		}
	}

}