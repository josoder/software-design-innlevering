﻿using System;
using System.Threading;

/* This is the same example code used in RaceConditions_example, but this time we have added locks. You still cant 
 * foresee the output, but you wont get an answer like for instance: " winter fall winter summer winter spring". 
 * but each thread will wait on the other thread to finish before it prints. That way we can get an answer like this:
 * "winter winter winter winter fall fall fall fall summer summer summer summer spring spring spring". 
 */


namespace Locks
{
	class MainClass
	{
		private static object theLock = new object();
		private static int count;
		static void Main(string[] args)
		{
			Thread Thread1 = new Thread(winter);
			Thread1.Start();

			Thread Thread2 = new Thread(spring);
			Thread2.Start();

			Thread Thread3 = new Thread(summer);
			Thread3.Start();

			Thread Thread4 = new Thread(fall);
			Thread4.Start();

			Console.ReadLine();
		}

		private static void winter()
		{
			lock (theLock)
			{
				for (count = 0; count < 20; count++)
				{
					Console.Write(" Winter " + "\t");
				}
			}
		}

		private static void spring()
		{
			lock (theLock)
			{
				for (count = 0; count < 20; count++)
				{
					Console.Write(" Spring " + "\t");
				}
			}
		}

		private static void summer()
		{
			lock (theLock)
			{
				for (count = 0; count < 20; count++)
				{
					Console.Write(" Summer " + "\t");
				}
			}
		}

		private static void fall()
		{
			lock (theLock)
			{
				for (count = 0; count < 20; count++)
				{
					Console.Write(" Fall " + "\t");
				}
			}
		}
	}
}
